# Singapore's Noisy Expressways

Forum posters in Singapore have complained that the value of their HDB flats suffer because they are near expressways, which are very noisy. Others say expressway proximity is good, due to the unblocked view. Is there merit to this view?

A hedonic price model using ordinary least squares regression is used to investigate this claim using data from public sources to examine the effect of proximity to Singapore's expressways on HDB flat resale prices.


### Data
* Resale flat transactions - https://data.gov.sg/dataset/resale-flat-prices
* Singapore Road / Expressway Geospatial data - https://data.gov.sg/dataset/national-map-line
* OneMap API For Geocoding Addresses - https://data.gov.sg/dataset/national-map-line

![alt text](static/img/whole_island.png "Whole Island Viz D3")

### Run Visualization
To interact with visualization (D3.js), run the following command:

```{bash}
$ python app.py
```
And go to local server http://127.0.0.1:5000


### Workflow
The code and analysis of this study is conducted in parts in the order of the following Jupyter Notebooks

[0 - Extracting Expressway Coordinates.ipynb](./0%20-%20Extracting%20Expressway%20Coordinates.ipynb)

[1 - Geocoding HDB Flats + MRT Stations.ipynb](./1%20-%20Geocoding%20HDB%20Flats%20+%20MRT%20Stations.ipynb)

[2 - Generate Shortest Distances to Expressway & MRT](./2%20-%20Generate%20Shortest%20Distances%20to%20Expressway%20%26%20MRT.ipynb)

[3 - Merge & Clean Datasets](./3%20-%20Merge%20%26%20Clean%20Datasets.ipynb)

[4 - Descripive Statistics](./4%20-%20Descripive%20Statistics.ipynb)

[5 - Inference](./5%20-%20Inference.ipynb)
